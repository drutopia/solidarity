# Solidarity

Solidarity is an online [platform cooperative](https://www.wikiwand.com/en/Platform_cooperative) designed by grassroots organizers to help one another act, mobilize and support one another.

The platform is built around four main features: groups, campaigns, actions and news.

[View the Solidarity Prototype](https://marvelapp.com/27hgh3d/screen/28546626)

A hosted option is available for $50/month which provides membership to Solidarity, covers security updates for the site and priority in determing the roadmap for future development.

If you are interested in joining or contributing to Solidarity, we'd love to hear from you - info@drutopia.org

Learn more about the project on the [Solidarity documentation page](http://docs.drutopia.org/en/latest/solidarity.html).






